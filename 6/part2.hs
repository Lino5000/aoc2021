import Data.List
import Data.Matrix
import qualified Data.Text as T

transition :: Matrix Int
transition =
  fromLists
    [ [0, 1, 0, 0, 0, 0, 0, 0, 0]
    , [0, 0, 1, 0, 0, 0, 0, 0, 0]
    , [0, 0, 0, 1, 0, 0, 0, 0, 0]
    , [0, 0, 0, 0, 1, 0, 0, 0, 0]
    , [0, 0, 0, 0, 0, 1, 0, 0, 0]
    , [0, 0, 0, 0, 0, 0, 1, 0, 0]
    , [1, 0, 0, 0, 0, 0, 0, 1, 0]
    , [0, 0, 0, 0, 0, 0, 0, 0, 1]
    , [1, 0, 0, 0, 0, 0, 0, 0, 0]
    ]

matPow :: Matrix Int -> Int -> Matrix Int
matPow a 1 = a
matPow a e = multStd2 a $ matPow a (e - 1)

splitStringAt :: Char -> String -> [String]
splitStringAt sep = map T.unpack . T.splitOn (T.pack [sep]) . T.pack

collate :: Int -> [Int] -> [Int]
collate _ [] = []
collate cat xs = collate (cat - 1) rest ++ [length include]
  where
    (include, rest) = partition (== cat) xs

day :: Int -> [Int] -> [Int]
day iter =
  toList . multStd2 (matPow transition (iter - 1)) . fromList 9 1 . collate 9

solve :: [Int] -> Int
solve = sum . day 256

main :: IO ()
main = getContents >>= print . solve . map read . splitStringAt ','
