import qualified Data.Text as T

splitStringAt :: Char -> String -> [String]
splitStringAt sep = map T.unpack . T.splitOn (T.pack [sep]) . T.pack

progressFish :: Int -> [Int]
progressFish 0 = [6, 8]
progressFish x = [x - 1]

day :: Int -> [Int] -> [Int]
day 0 fish = fish
day iter fish = concatMap progressFish $ day (iter - 1) fish

solve :: [Int] -> Int
solve = length . day 80

main :: IO ()
main = getContents >>= print . solve . map read . splitStringAt ','
