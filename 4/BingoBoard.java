public class BingoBoard {
  private int[][] board;
  private boolean[][] marks;
  private int lastDraw;

  public BingoBoard(int[][] board) {
    this.board = board;
    this.marks = new boolean[board.length][board[0].length];
  }

  public void mark(int number) {
    for (int rowi = 0; rowi < this.board.length; rowi++) {
      for (int coli = 0; coli < this.board[0].length; coli++) {
        if (this.board[rowi][coli] == number) {
          this.marks[rowi][coli] = true;
        }
      }
    }
    this.lastDraw = number;
  }

  public boolean hasWon() {
    boolean[] columnWins = new boolean[this.marks[0].length];
    // Set all the columns to true
    for (int coli = 0; coli < columnWins.length; coli++) {
      columnWins[coli] = true;
    }
    for (boolean[] row : this.marks) {
      boolean rowWin = true;
      for (int coli = 0; coli < columnWins.length; coli++) {
        rowWin &= row[coli];
        columnWins[coli] &= row[coli];
      }
      if (rowWin) {
        return true;
      }
    }

    for (boolean colWin : columnWins) {
      if (colWin) {
        return true;
      }
    }

    // Diagonals don't count
    return false;
  }

  public int getScore() {
    int total = 0;
    for (int rowi = 0; rowi < this.board.length; rowi++) {
      for (int coli = 0; coli < this.board[0].length; coli++) {
        if (!this.marks[rowi][coli]) {
          total += this.board[rowi][coli];
        }
      }
    }
    return this.lastDraw * total;
  }

  public String toString() {
    StringBuilder out = new StringBuilder();
    for (int[] row : this.board) {
      for (int d : row) {
        out.append(String.format("%3d", d));
      }
      out.append('\n');
    }
    return out.toString();
  }

  public void printMarks() {
    for (boolean[] row : this.marks) {
      for (boolean m : row) {
        System.out.printf("%2s", m ? "*" : " ");
      }
      System.out.println();
    }
  }
}
