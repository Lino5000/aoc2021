import java.util.ArrayList;
import java.util.Scanner;

public class Part1 {
  public static void main(String[] args) {
    Scanner stdin = new Scanner(System.in);

    // First line is the draws
    String[] drawsStr = stdin.nextLine().split(",");
    int[] draws = new int[drawsStr.length];
    for (int i = 0; i < drawsStr.length; i++) {
      draws[i] = Integer.parseInt(drawsStr[i]);
    }

    stdin.nextLine();

    // Remaining lines indicate the 5x5 boards.
    ArrayList<BingoBoard> boards = new ArrayList<BingoBoard>();
    while (stdin.hasNextLine()) {
      int[][] board = new int[5][5];
      for (int rowi = 0; rowi < 5; rowi++) {
        for (int coli = 0; coli < 5; coli++) {
          board[rowi][coli] = stdin.nextInt();
        }
      }
      boards.add(new BingoBoard(board));

      // Remove the gap lines, except there isn't one at the end of the file.
      if (stdin.hasNextLine()) {
        stdin.nextLine();
      }
    }

    // Mark all of the draws
    boolean bingo = false;
    for (int d : draws) {
      for (BingoBoard board : boards) {
        board.mark(d);
        if (board.hasWon()) {
          bingo = true;
          // Print the winning board
          System.out.print(board);
          board.printMarks();
          System.out.println("Score = " + board.getScore());
          break;
        }
      }
      if (bingo) {
        break;
      }
    }
  }
}
