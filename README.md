# AOC2021

This repository is where I am storing my solutions to the [Advent Of
Code](https://adventofcode.com/) problems for 2021. They likely won't all get
done on the correct day, but I do want to try and complete the challenges
before the end of the day when I can, so we'll see what happens.

_Update - 3/12/2021: That failed pretty quickly, but to be fair I had a Uni
exam today, so missing yesterday is fair enough. Whether I have time to catch
up today remains to be seen._

_Update - 04/12/2021: I have caught up with yesterday's problems, just waiting
for today's to arrive in a couple hours. I am busy tomorrow, so might not
manage that one on time, but we'll see._
