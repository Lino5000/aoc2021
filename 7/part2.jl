function cost(target::Int, initial::Vector{Int}) :: Int
    return sum(map(initial) do p
                   n = abs(p - target)
                   n * (n+1) / 2
               end)
end

function solve(input::Vector{Int})
    return minimum(x->(cost(x, input), x), range(extrema(input)..., step=1))
end

open(ARGS[1], "r") do file
    line = readline(file)
    println(solve(map(s->parse(Int, s), split(line, ","))))
end
