function flood_fill(grid::Array{Int, 2}, boundary::Int, seed::Tuple{Int, Int}) :: Int
    # Returns the size of the region in grid that contains seed and is
    # surrounded by entries with the boundary value.

    stack::Vector{Tuple{Int, Int}} = [seed]

    visited = falses(size(grid)...)

    while length(stack) > 0
        (i, j) = pop!(stack)
        visited[i, j] = true

        # Above
        if i > 1 && !visited[i-1, j] && grid[i-1, j] != boundary
            push!(stack, (i-1, j))
        end
        # Below
        if i < size(grid, 1) && !visited[i+1, j] && grid[i+1, j] != boundary
            push!(stack, (i+1, j))
        end
        # Left
        if j > 1 && !visited[i, j-1] && grid[i, j-1] != boundary
            push!(stack, (i, j-1))
        end
        # Right
        if j < size(grid, 2) && !visited[i, j+1] && grid[i, j+1] != boundary
            push!(stack, (i, j+1))
        end
    end

    length(findall(visited))
end

function solve(grid::Array{Int, 2}) :: Int
    low_points = Vector{Tuple{Int, Int}}(undef, 0)

    for i ∈ axes(grid, 1), j ∈ axes(grid, 2)
        # Is this index a low point?
        low_point = true

        value = grid[i,j]

        # Above
        if i > 1
            low_point = low_point && (grid[i-1,j] > value)
        end
        # Below
        if i < size(grid, 1)
            low_point = low_point && (grid[i+1,j] > value)
        end
        # Left
        if j > 1
            low_point = low_point && (grid[i,j-1] > value)
        end
        # Right
        if j < size(grid, 2)
            low_point = low_point && (grid[i,j+1] > value)
        end

        if low_point
            # It is a low point
            push!(low_points, (i, j))
        end
    end

    region_sizes = map(low_points) do p
        flood_fill(grid, 9, p)
    end

    # Find the largest three regions and take their product
    prod(sort(region_sizes, rev=true)[1:3])
end

if length(ARGS) < 1
    println("Please provide an input file.")
else
    open(ARGS[1], "r") do file
        grid = map(readlines(file)) do line
            map(s->parse(Int, s), split(line, ""))
        end
        println(solve(permutedims(hcat(grid...), (2, 1))))
    end
end
