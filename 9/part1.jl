function solve(grid::Array{Int, 2}) :: Int
    total = 0

    for i ∈ axes(grid, 1), j ∈ axes(grid, 2)
        # Is this index a low point?
        low_point = true

        value = grid[i,j]

        # Above
        if i > 1
            low_point = low_point && (grid[i-1,j] > value)
        end
        # Below
        if i < size(grid, 1)
            low_point = low_point && (grid[i+1,j] > value)
        end
        # Left
        if j > 1
            low_point = low_point && (grid[i,j-1] > value)
        end
        # Right
        if j < size(grid, 2)
            low_point = low_point && (grid[i,j+1] > value)
        end

        if low_point
            # It is a low point
            total += value + 1
        end
    end

    total
end

if length(ARGS) < 1
    println("Please provide an input file.")
else
    open(ARGS[1], "r") do file
        grid = map(readlines(file)) do line
            map(s->parse(Int, s), split(line, ""))
        end
        println(solve(permutedims(hcat(grid...), (2, 1))))
    end
end
