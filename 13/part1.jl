function fold(grid::Array{Bool, 2}, fold::Tuple{Bool, Int}) :: Array{Bool, 2}
    (dir, pos) = fold
    if dir
        # Vertical
        new_width = pos - 1
        new_height = size(grid, 2)
    else
        # Horizontal
        new_width = size(grid, 1)
        new_height = pos - 1
    end

    new_grid = Array{Bool, 2}(undef, (new_width, new_height))
    region = CartesianIndices(new_grid)
    copyto!(new_grid, region, grid, region)

    for (ind, elem) ∈ pairs(new_grid)
        if dir
            fold_ind = CartesianIndex(2 * pos - ind[1], ind[2])
        else
            fold_ind = CartesianIndex(ind[1], 2 * pos - ind[2])
        end
        new_grid[ind] = elem || grid[fold_ind]
    end

    new_grid
end

function build_grid(points::Vector{Tuple{Int, Int}}) :: Array{Bool, 2}
    width = maximum(p->p[1], points)
    height = maximum(p->p[2], points)
    grid = zeros(Bool, width, height)

    map(points) do point
        grid[point...] = true
    end

    grid
end

function parse_file(filename::String) :: Tuple{Vector{Tuple{Int, Int}}, Vector{Tuple{Bool, Int}}}
    open(filename, "r") do file
        parsing_points = true
        points = Vector{Tuple{Int, Int}}(undef, 0)
        folds = Vector{Tuple{Bool, Int}}(undef, 0)

        map(readlines(file)) do line
            if parsing_points
                if line == ""
                    parsing_points = false
                else
                    (x, y) = map(s->parse(Int, s) + 1, split(line, ","))
                    push!(points, (y, x))
                end
            else
                pieces = split(replace(line, "fold along "=>""), "=")
                push!(folds, (pieces[1] == "y", parse(Int, pieces[2]) + 1))
            end
        end

        (points, folds)
    end
end

function solve(filename::String) :: Int
    (points, folds) = parse_file(filename)
    #println(points)

    grid = build_grid(points)
    #display(grid)
    #println()

    first_fold = fold(grid, folds[1])
    #display(first_fold)
    #println()

    count(first_fold)
end

if length(ARGS) < 1
    println("Please provide an input file.")
else
    println(solve(ARGS[1]))
end
