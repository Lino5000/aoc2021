"""Part 1 of Day 10 of AOC2021"""

import sys

def opening(char):
    """Check if the given character can open a chunk."""
    return char in ['(', '[', '{', '<']

def closed_by(char):
    """Get the opening character that matches the given closing character."""
    if char == ')':
        return '('
    if char == ']':
        return '['
    if char == '}':
        return '{'
    if char == '>':
        return '<'
    return ''

def check_line(line):
    """Check whether the line is valid. Returns a tuple (isValid, errorCharacter)."""
    stack = []
    for char in line:
        if opening(char):
            stack.append(char)
        elif stack.pop() != closed_by(char):
            return (False, char)
    return (True, None)

def check_file(filepath):
    """Check all the lines of a file, and return a dictionary containing the count for each error"""
    with open(filepath, 'r') as file:
        counts = {}
        for line in file:
            (is_valid, err) = check_line(line.strip())
            if not is_valid:
                counts[err] = 1 + counts.get(err, 0)
    return counts

def calc_score(counts):
    """Calculate the score for the given counts"""
    return 3 * counts.get(')', 0) + \
            57 * counts.get(']', 0) + 1197 * counts.get('}', 0) + 25137 * counts.get('>', 0)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        print(calc_score(check_file(sys.argv[1])))
    else:
        print(f"Usage: python {sys.argv[0]} <filename>")
