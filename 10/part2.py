"""Part 1 of Day 10 of AOC2021"""

import sys

def opening(char):
    """Check if the given character can open a chunk."""
    return char in ['(', '[', '{', '<']

def closed_by(char):
    """Get the opening character that matches the given closing character."""
    if char == ')':
        return '('
    if char == ']':
        return '['
    if char == '}':
        return '{'
    if char == '>':
        return '<'
    return ''

def opened_by(char):
    """Get the closing character that matches the given opening character."""
    if char == '(':
        return ')'
    if char == '[':
        return ']'
    if char == '{':
        return '}'
    if char == '<':
        return '>'
    return ''

def check_line(line):
    """
    Check whether the line is valid.

    If the line is corrupt, returns (False, incorrect_char).
    If the line is valid, or incomplete, returns (True, completion)
    """
    stack = []
    for char in line:
        if opening(char):
            stack.append(char)
        elif stack.pop() != closed_by(char):
            return (False, char)
    return (True, list(map(opened_by, reversed(stack))))

def check_file(filepath):
    """Check all the lines of a file, and return a dictionary containing the count for each error"""
    with open(filepath, 'r') as file:
        completions = []
        for line in file:
            (is_valid, note) = check_line(line.strip())
            if is_valid:
                completions.append(note)
    return completions

SCORES = {')': 1, ']': 2, '}': 3, '>': 4}

def calc_score(completion):
    """Calculate the score for the given completion"""
    score = 0
    for char in completion:
        score = score * 5 + SCORES[char]
    return score

def main():
    """The main script"""
    if len(sys.argv) == 2:
        line_scores = list(sorted(map(calc_score, check_file(sys.argv[1]))))
        print(line_scores[len(line_scores)//2])
    else:
        print(f"Usage: python {sys.argv[0]} <filename>")

if __name__ == '__main__':
    main()
