{-# LANGUAGE FlexibleInstances #-}

import Control.Applicative
import Data.Char

data Chunk
  = ParenChunk [Chunk]
  | BraceChunk [Chunk]
  | AngleChunk [Chunk]
  | BrackChunk [Chunk]
  | Empty
  deriving (Show)

newtype ParserError =
  ParserError String
  deriving (Show)

newtype Parser a =
  Parser
    { runParser :: String -> Either ParserError (String, a)
    }

instance Functor Parser where
  fmap f (Parser p) =
    Parser $ \input -> do
      (input', x) <- p input
      Right (input', f x)

instance Applicative Parser where
  pure x = Parser $ \input -> Right (input, x)
  (Parser p1) <*> (Parser p2) =
    Parser $ \input -> do
      (input', f) <- p1 input
      (input'', a) <- p2 input'
      Right (input'', f a)

instance Alternative (Either ParserError) where
  empty = Left $ ParserError "Empty"
  Left _ <|> p2 = p2
  p1 <|> _ = p1

instance Alternative Parser where
  empty = Parser $ const empty
  (Parser p1) <|> (Parser p2) = Parser $ \input -> p1 input <|> p2 input

charP :: Char -> Parser Char
charP x = Parser f
  where
    f (y:ys)
      | y == x = Right (ys, x)
      | otherwise = Left $ ParserError (y : ys)
    f _ = empty

emptyP :: Parser [Chunk]
emptyP = Parser $ \input -> Right (input, [Empty])

buildChunkP :: Char -> Parser [Chunk]
buildChunkP s = charP s *> (some chunkP <|> emptyP) <* charP (closing s)

parenChunkP :: Parser Chunk
parenChunkP = ParenChunk <$> buildChunkP '('

braceChunkP :: Parser Chunk
braceChunkP = BraceChunk <$> buildChunkP '{'

angleChunkP :: Parser Chunk
angleChunkP = AngleChunk <$> buildChunkP '<'

brackChunkP :: Parser Chunk
brackChunkP = BrackChunk <$> buildChunkP '['

chunkP :: Parser Chunk
chunkP = parenChunkP <|> braceChunkP <|> angleChunkP <|> brackChunkP

eolP :: Parser [Chunk]
eolP = Parser f
  where
    f [] = Right ("", [Empty])
    f xs = Left $ ParserError xs

lineP :: Parser [Chunk]
lineP = some chunkP <* eolP

closing :: Char -> Char
closing '(' = ')'
closing '[' = ']'
closing '{' = '}'
closing '<' = '>'
closing _ = chr 0

parseLine :: String -> Either Char [Chunk]
parseLine = res . runParser lineP
  where
    res :: Either ParserError (String, [Chunk]) -> Either Char [Chunk]
    res (Left (ParserError msg)) = Left $ findError msg
    res (Right (_, cs)) = Right cs

findError :: String -> Char
findError badString =
  case runParser chunkP badString of
    Right (rest, _) -> findError rest
    Left (ParserError [s, e]) -> e
    Left (ParserError msg) -> head . map findError $ splitChunks msg

-- No clue how to implement this...
splitChunks :: String -> [String]
splitChunks msg = undefined

main :: IO ()
main = undefined
