solve :: [Int] -> Int
solve xs = sum . zipWith f xs $ tail xs
  where
    f a b =
      if a < b
        then 1
        else 0

main :: IO ()
main = getContents >>= print . solve . map read . lines
