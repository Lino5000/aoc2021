windowSums :: [Int] -> [Int]
windowSums xs = zipWith (+) xs $ zipWith (+) (tail xs) (drop 2 xs)

solve :: [Int] -> Int
solve xs = sum . zipWith f windows $ tail windows
  where
    windows = windowSums xs
    f a b =
      if a < b
        then 1
        else 0

main :: IO ()
main = getContents >>= print . solve . map read . lines
