"""Messing around with possibilities"""

import sys
import pprint

def main():
    """Main program"""
    if len(sys.argv) > 1:
        with open(sys.argv[1], 'r') as file:
            lines = list(map(lambda s: s.strip().split(" -> "), file.readlines()))
            # template = lines[0]
            char_2_index = {}
            # Determine how many unique characters there are
            index = 0
            for insertion in lines[2:]:
                a, b = list(insertion[0])
                if a not in char_2_index:
                    char_2_index[a] = index
                    index += 1
                if b not in char_2_index:
                    char_2_index[b] = index
                    index += 1

            # Build the table of transitions
            table = [["" for _ in range(index)] for _ in range(index)]
            for insertion in lines[2:]:
                a, b = list(insertion[0])
                i, j = char_2_index[a], char_2_index[b]
                table[i][j] = insertion[1]

            pprint.pprint(table)
    else:
        print("Please provide an input file.")

if __name__ == "__main__":
    main()
