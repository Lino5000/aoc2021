import Data.List (group, sort)
import Data.Map (Map, (!), fromList)

parseInput :: [String] -> (String, Map (Char, Char) Char)
parseInput (template:_:insertions) = (template, parseInsertions insertions)
  where
    parseInsertions :: [String] -> Map (Char, Char) Char
    parseInsertions = fromList . map (\(a:b:cs) -> ((a, b), last cs))

applyInsertions :: Map (Char, Char) Char -> String -> String
applyInsertions mapping template =
  (++ [last template]) .
  concatMap (\(a, b) -> [a, mapping ! (a, b)]) . zip template $
  tail template

solve :: Int -> [String] -> Int
solve iters lines = maximum counts - minimum counts
  where
    counts = map length . group $ sort polymer
    polymer = foldl (\a _ -> applyInsertions mapping a) temp [0 .. iters - 1]
    (temp, mapping) = parseInput lines

main :: IO ()
main = getContents >>= print . solve 10 . lines
