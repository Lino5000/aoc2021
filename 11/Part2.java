import java.util.Scanner;
import java.util.ArrayList;

public class Part2 {
  private Octopus[][] grid;

  public int runSimulation() {
    // Run the simulation until the first synchronous flash, then return how
    // many iterations it takes.
    int iters = 0;

    int needed_flashes = this.grid.length * this.grid[0].length;

    while (this.simulationStep() != needed_flashes) {
      iters++;
    }

    return iters;
  }

  public void readGrid(Scanner inp) {
    ArrayList<String> lines = new ArrayList<String>();
    while (inp.hasNextLine()) {
      lines.add(inp.nextLine());
    }

    this.grid = new Octopus[lines.size()][lines.get(0).length()];

    for (int row = 0; row < lines.size(); row++) {
      char[] line = lines.get(row).toCharArray();
      for (int col = 0; col < line.length; col++) {
        this.grid[row][col] = new Octopus(Character.digit(line[col], 10));
      }
    }
  }

  public String toString() {
    return stringMatrix(this.grid);
  }

  private int simulationStep() {
    // Run a single time-step of the smulation. Returns the number of flashes
    // that occur in this time step.
    int flash_count = 0;

    // Add Energy to every octopus.
    for (int rowi = 0; rowi < this.grid.length; rowi++) {
      for (int coli = 0; coli < this.grid[rowi].length; coli++) {
        flash_count += this.addEnergy(rowi, coli);
      }
    }

    // Reset anything that flashed.
    for (Octopus[] row : this.grid) {
      for (Octopus o : row) {
        o.reset();
      }
    }

    return flash_count;
  }

  private int addEnergy(int rowi, int coli) {
    // Add energy to the octopus. If it flashes, add energy to its neighbours.
    // Returns the number of flashes that occur due to this addition of energy.

    int flash_count = 0;
    if (this.grid[rowi][coli].addEnergy()) {
      // The octopus flashed
      flash_count += 1;

      int rmin, rmax, cmin, cmax;

      // Check whether we're on an edge;
      if (rowi == 0) {
        rmin = 0;
      } else {
        rmin = -1;
      }
      if (rowi == this.grid.length - 1) {
        rmax = 1;
      } else {
        rmax = 2;
      }
      if (coli == 0) {
        cmin = 0;
      } else {
        cmin = -1;
      }
      if (coli == this.grid[rowi].length - 1) {
        cmax = 1;
      } else {
        cmax = 2;
      }

      // Add energy to the neighbours
      for (int roff = rmin; roff < rmax; roff++) {
        for (int coff = cmin; coff < cmax; coff++) {
          if (roff != 0 || coff != 0) { // Don't re-flash this one
            flash_count += this.addEnergy(rowi + roff, coli + coff);
          }
        }
      }
    }

    return flash_count;
  }

  public static void main(String[] args) {
    Scanner stdin = new Scanner(System.in);

    Part2 sim = new Part2();

    sim.readGrid(stdin);

    System.out.println(sim);

    int iters = sim.runSimulation() + 1;

    System.out.println(sim);
    System.out.println("It took " + iters + " iterations.");
  }

  public static <T> String stringMatrix(T[][] matrix) {
    StringBuilder out = new StringBuilder();

    for (T[] row : matrix) {
      for (T obj : row) {
        out.append(obj);
      }
      out.append("\n");
    }

    return out.toString();
  }
}
