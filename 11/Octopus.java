public class Octopus {
  private int energy;
  private boolean hasFlashed;

  public Octopus(int energy) {
    this.energy = energy;
    this.hasFlashed = false;
  }

  public boolean addEnergy() {
    this.energy += 1;
    if (this.energy > 9) {
      return this.flash();
    }
    return false;
  }

  public void reset() {
    if (this.hasFlashed) {
      this.energy = 0;
      this.hasFlashed = false;
    }
  }

  public String toString() {
    return String.format("%d", this.energy);
  }

  private boolean flash() {
    if (!this.hasFlashed) {
      this.hasFlashed = true;
      return true;
    }
    return false;
  }
}
