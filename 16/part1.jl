abstract type Packet end

struct Literal <: Packet
    version::UInt8
    type::UInt8
    value::Int  # Only meaningful if type == 4
end

struct Operator <: Packet
    version::UInt8
    type::UInt8
    length_type::Bool  # Remaining fields are only meaningful if type != 4
    length::Int
    sub_packets::Vector{Packet}
end

const VERSION_MASK = 0b11100000
const TYPE_MASK = 0b00011100

function get_bits(bytes::Vector{UInt8}, start::Unsigned, len::Unsigned) :: UInt
    # Retrive `length` bits starting from `start` as a 0-index,
    # left-to-right position in the binary representation of 
    # `bytes` concatenated.
    start_offset = (start % 8)

    added_zero = false

    if (start + len) ÷ 8 + 1 > length(bytes)
        push!(bytes, 0x0)
        added_zero = true
    end
    bytes_of_interest = bytes[start ÷ 8 + 1 : (start + len) ÷ 8 + 1]

    if added_zero
        pop!(bytes)
    end

    value = 0
    for byte ∈ bytes_of_interest
        value = (value << 8) | byte
    end

    shift = length(bytes_of_interest) * 8 - start_offset - len

    mask = (2 ^ len - 1) << shift

    (mask & value) >>> shift
end

function shift_left(bytes::Vector{UInt8}, amount::Unsigned) :: Vector{UInt8}
    out = bytes

    while amount > 8
        push!(out, 0x0)
        amount -= 8
    end

    if amount > 0
        carry_in = 0x0
        for i ∈ length(out):-1:1
            carry_out = UInt8(out[i] >>> (8 - amount))
            out[i] = UInt8(out[i] << amount) | carry_in
            carry_in = carry_out
        end
    end

    out 
end

function get_remaining(bytes::Vector{UInt8}, bit_idx::Unsigned) :: Vector{UInt8}
    out = bytes[bit_idx ÷ 8 + 1 : length(bytes)]

    mask = 2 ^ (8 - bit_idx % 8) - 1

    out[1] = out[1] & mask

    shift_left(out, UInt(bit_idx % 8))
end

function decode_packet(bytes::Vector{UInt8}, bit_idx::Unsigned) :: Tuple{Packet, UInt}
    start_bit = copy(bit_idx)
    if debug print("(bit " * string(bit_idx) * ", ") end
    version = get_bits(bytes, bit_idx, 0x3)
    bit_idx += 0x3
    if debug print("version " * string(version) * ", ") end

    type = get_bits(bytes, bit_idx, 0x3)
    bit_idx += 0x3
    if debug print("type " * string(type) * ", ") end
    if type == 4
        # Literal value
        value = 0
        keep_checking = true

        while keep_checking
            bits = get_bits(bytes, bit_idx, 0x5)
            keep_checking = bits >>> 4 ≠ 0
            bit_idx += 0x5

            value = (value << 4) | (bits & 0xf)
        end
        
        if debug println("lit " * string(value) * ")") end

        (Literal(version, type, value), bit_idx - start_bit)
    else
        # Operator with Sub-packets
        if debug print("op ") end

        length_type = get_bits(bytes, bit_idx, 0x1) ≠ 0
        bit_idx += 0x1
        if debug print("len_type " * string(length_type) * ", ") end

        len = 0
        subpackets = Packet[]

        if length_type
            # 11 bits for number of sub-packets
            len = get_bits(bytes, bit_idx, 0xb)
            bit_idx += 0xb
            if debug println("len " * string(len) * ")") end

            if debug println("---- subpackets for (" * string(version) * "," * string(type) * ") ----") end
            for i ∈ 1:len
                (packet, used) = decode_packet(bytes, bit_idx)
                bit_idx += used
                push!(subpackets, packet)
            end
            if debug println("---- end subpackets for (" * string(version) * "," * string(type) * ") ----") end
        else
            # 15 bits for total bits in sub-packets
            len = get_bits(bytes, bit_idx, 0xf)
            bit_idx += 0xf
            if debug println("len " * string(len) * ")") end

            if debug println("---- subpackets for (" * string(version) * "," * string(type) * ") ----") end
            subpacket_bits = 0
            while subpacket_bits < len
                (packet, used) = decode_packet(bytes, bit_idx)
                bit_idx += used
                subpacket_bits += used
                push!(subpackets, packet)
            end
            if debug println("---- end subpackets for (" * string(version) * "," * string(type) * ") ----") end
        end
        
        (Operator(version, type, length_type, len, subpackets), bit_idx - start_bit)
    end
end

function version_sum(pack::Packet) :: UInt
    total = pack.version

    if pack.type ≠ 4
        for subpack ∈ pack.sub_packets
            total += version_sum(subpack)
        end
    end

    total
end

function parse_file(filename::String) :: Vector{UInt8}
    open(filename, "r") do file
        out = UInt8[]
        line = readline(file)
        for i ∈ 1:2:length(line)
            push!(out, parse(UInt8, line[i:i+1], base=16))
        end

        out
    end
end

function solve(filename::String) :: UInt
    bytes = parse_file(filename)

    (pack, bits_used) = decode_packet(bytes, 0x0)

    version_sum(pack)
end

const debug = false

if length(ARGS) < 1
    println("Please provide an input file.")
else
    for arg ∈ ARGS
        println(arg)
        println(solve(arg))
        println()
    end
end
