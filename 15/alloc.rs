use std::ops::{ Index, IndexMut, Deref, DerefMut };
use std::cmp::{ PartialEq, PartialOrd, Ord, Ordering };

#[derive(Debug, Clone, Copy)]
pub struct Pointer<T: Default + Ord> {
    pub idx: usize,
    alloc: *mut Allocator<T>,
}

impl<T: Default + Ord> Deref for Pointer<T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &self.alloc.as_ref().unwrap().pool[self.idx] }
    }
}

impl<T: Default + Ord> DerefMut for Pointer<T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut self.alloc.as_mut().unwrap().pool[self.idx] }
    }
}

impl<T: Default + Ord> PartialEq<Pointer<T>> for Pointer<T> {
    fn eq(&self, other: &Pointer<T>) -> bool {
        **self == **other
    }
}

impl<T: Default + Ord> Eq for Pointer<T> {}

impl<T: Default + Ord> PartialOrd<Pointer<T>> for Pointer<T> {
    fn partial_cmp(&self, other: &Pointer<T>) -> Option<Ordering> {
        (**self).partial_cmp(&**other)
    }
}

impl<T: Default + Ord> Ord for Pointer<T> {
    fn cmp(&self, other: &Pointer<T>) -> Ordering {
        (**self).cmp(&**other)
    }
}

#[derive(Default, Debug)]
pub struct Allocator<T: Default + Ord> {
    pool: Vec<T>,
}

impl<T: Default + Ord> Allocator<T> {
    pub fn alloc(&mut self) -> Pointer<T> {
        self.pool.push(T::default());
        Pointer{ idx: self.pool.len() - 1, alloc: self }
    }

    pub fn size(&self) -> usize {
        self.pool.len()
    }

    pub fn get_ptr(&mut self, idx: usize) -> Pointer<T> {
        Pointer{ idx, alloc: self }
    }

    pub fn get_last_ptr(&mut self) -> Pointer<T> {
        self.get_ptr(self.size() - 1)
    }

    pub fn fix_pointer(&mut self, ptr: &Pointer<T>) -> Pointer<T> {
        Pointer {
            alloc: self,
            ..*ptr
        }
    }
}

impl<T: Default + Ord> Index<Pointer<T>> for Allocator<T> {
    type Output = T;
    fn index(&self, idx: Pointer<T>) -> &T {
        &self.pool[idx.idx]
    }
}

impl<T: Default + Ord> IndexMut<Pointer<T>> for Allocator<T> {
    fn index_mut(&mut self, idx: Pointer<T>) -> &mut T {
        &mut self.pool[idx.idx]
    }
}
