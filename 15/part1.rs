use std::io::{ stdin, BufRead, Lines };
use std::ops::{ Index, IndexMut };
use std::collections::BinaryHeap;
use std::cmp::{ Reverse, Ordering };
use std::fmt;

mod alloc;
use alloc::*;

#[derive(Debug, Clone)]
struct Point {
    row: usize,
    col: usize,
    cost: u32,
    previous: Option<Pointer<Point>>,
    path_cost: u32,
    f_cost: u32,
}

impl Default for Point {
    fn default() -> Self {
        Self {
            row: 0,
            col: 0,
            cost: 0,
            previous: None,
            path_cost: u32::MAX,
            f_cost: u32::MAX,
        }
    }
}

impl PartialEq<Point> for Point {
    fn eq(&self, other: &Self) -> bool {
        self.row == other.row && self.col == other.col
    }
}

impl Eq for Point {}

impl PartialOrd<Point> for Point {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.f_cost.partial_cmp(&other.f_cost)
    }
}

impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        self.f_cost.cmp(&other.f_cost)
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.row, self.col)
    }
}

impl Point {
    fn heur(&self, other: &Point) -> u32 {
        // Manhattan Distance to the given point.
        (i32::abs(self.row as i32 - other.row as i32)
         + i32::abs(self.col as i32 - other.col as i32)) as u32
    }
}

#[derive(Debug, Default)]
struct Paths {
    alloc: Allocator<Point>,
    board: Vec<Vec<Pointer<Point>>>,
    width: usize,
    target: Option<Pointer<Point>>,
}

//         ( row ,  col ) indexing
impl Index<(usize, usize)> for Paths {
    type Output = Point;
    fn index(&self, idx: (usize, usize)) -> &Point {
        &self.alloc[self.to_pointer(idx)]
    }
}

//         ( row ,  col ) indexing
impl IndexMut<(usize, usize)> for Paths {
    fn index_mut(&mut self, idx: (usize, usize)) -> &mut Point {
        let ptr = self.to_pointer(idx);
        &mut self.alloc[ptr]
    }
}

impl Paths {
    fn parse_input(lines: Lines<impl BufRead>) -> Self {
        let mut alloc: Allocator<Point> = Allocator::default();
        let mut board: Vec<Vec<Pointer<Point>>> = Vec::new();

        let mut row = 0;
        let mut col = 0;

        let mut width = 0;

        for line in lines {
            let mut board_row: Vec<Pointer<Point>> = Vec::new();
            for c in line.unwrap().chars() {
                if let Some(val) = c.to_digit(10) {
                    let mut p = alloc.alloc();
                    *p = Point {
                        row, col,
                        cost: val,
                        ..Default::default()
                    };
                    board_row.push(p);
                }
                col += 1;
            }
            width = col;
            row += 1;
            col = 0;

            board.push(board_row);
        }

        let target = Some(alloc.get_last_ptr());

        Self { alloc, board, width, target }
    }

    fn get_path(&self) -> Vec<Point>{
        self.trace_back(self.target.clone().unwrap())
    }

    fn get_path_cost(&self) -> u32 {
        self.alloc[self.target.clone().unwrap()].path_cost
    }

    fn trace_back(&self, end: Pointer<Point>) -> Vec<Point> {
        let mut cursor = end;
        // println!("\nStarting backtrack from: {:?}", cursor);
        let mut out = vec![];
        while let Some(next_ptr) = cursor.previous.clone() {
            out.push((*cursor).clone());
            cursor = next_ptr;
            // println!("  Previous: {:?}", cursor);
        }

        out.push((*cursor).clone()); // Add the (0, 0) start

        out.reverse();

        out
    }

    fn to_pointer(&self, idx: (usize, usize)) -> Pointer<Point> {
        let (row, col) = idx;
        self.board[row][col].clone()
    }

    fn neighbours(&self, pos: (usize, usize)) -> Vec<Pointer<Point>> {
        let mut out: Vec<Pointer<Point>> = vec![];

        let (row, col) = pos;

        // Above
        if row > 0 {
            out.push(self.to_pointer((row-1, col)));
        }
        // Below
        if row < self.width - 1 {
            out.push(self.to_pointer((row+1, col)));
        }
        // Left
        if col > 0 {
            out.push(self.to_pointer((row, col-1)));
        }
        // Right
        if col < self.alloc.size() / self.width - 1{
            out.push(self.to_pointer((row, col+1)));
        }

        out
    }

    fn find_path(&mut self) {
        // Implement A* path-finding
        let start = (0, 0);
        self[start].path_cost = 0; // Starting position risk is not counted.
        self[start].f_cost =
            self[start].heur(&self.alloc[self.target.clone().unwrap()]) + self[start].cost;

        let mut open_set: BinaryHeap<Reverse<Pointer<Point>>> = BinaryHeap::new();
        open_set.push(Reverse(self.to_pointer(start)));

        while !open_set.clone().is_empty() {
            if let Some(Reverse(current_ptr)) = open_set.pop() {
                let current_idx = (current_ptr.row, current_ptr.col);
                // println!("ptr: {:?}", current_ptr);
                let target = self.target.clone().unwrap();
                // println!("target: {:?}", target);
                if current_ptr == target {
                    return;
                }

                // println!("Considering {:?}:", current_ptr);

                for mut neighbour in self.neighbours(current_idx) {
                    // println!("    Checking neighbour: {:?}", neighbour);
                    let tentative_cost = current_ptr.path_cost + neighbour.cost;
                    if tentative_cost < neighbour.path_cost {
                        neighbour.previous = Some(current_ptr.clone());
                        neighbour.path_cost = tentative_cost;
                        neighbour.f_cost =
                            tentative_cost + neighbour.heur(&target);
                        // println!("        Updated: {:?}", neighbour);

                        if !min_heap_contains(&open_set, &neighbour) {
                            open_set.push(Reverse(neighbour));
                        }
                    }
                }
            } else {
                panic!("Popping a non-empty BinaryHeap gave a `None`??");
            }
        }

        panic!("There isn't a path ???");
    }

    fn fix_ptrs(&mut self) {
        for row in self.board.iter_mut() {
            for ptr in row {
                *ptr = self.alloc.fix_pointer(&ptr);
            }
        }

        //println!("{:?}", self.target);
        if let Some(t) = self.target.clone() {
            self.target = Some(self.alloc.fix_pointer(&t));
        }
        //println!("{:?}", self.target);
    }
}

fn min_heap_contains<T: PartialEq>(heap: &BinaryHeap<Reverse<T>>, elem: &T) -> bool {
    for Reverse(e) in heap.iter() {
        if e == elem {
            return true;
        }
    }
    false
}

fn main() {
    let stdin = stdin();
    let stdin = stdin.lock();
    let lines = stdin.lines();

    let mut board = Paths::parse_input(lines);

    board.fix_ptrs();
    board.find_path();
    let path = board.get_path();

    println!("\nPath is:");
    for p in path.iter() {
        println!("  {}", p);
    }

    println!("Cost of this path is: {}", board.get_path_cost());
}
