"""
Part 1 of Day 5 of the 2021 AOC.
"""

import sys

class VentGrid:
    """Represents a grid of vents"""
    width = 0
    height = 0
    board = []

    def add_line(self, x_1, y_1, x_2, y_2):
        """Add a line to the board"""
        # Check not diagonal
        if y_1 != y_2 and x_1 != x_2:
            # print(f"({x_1}, {y_1}) -> ({x_2}, {y_2}) is Diagonal")
            return
        # print(f"({x_1}, {y_1}) -> ({x_2}, {y_2}) Added")
        # Update width and height
        self.width = max(self.width, max(x_1, x_2) + 1)
        self.height = max(self.height, max(y_1, y_2) + 1)
        # Update board to this width & height
        if len(self.board) < self.height:
            self.board += [[] for _ in range(self.height - len(self.board))]
        for j in range(self.height):
            if len(self.board[j]) < self.height:
                self.board[j] += [0 for _ in range(self.width - len(self.board[j]))]
        # Add 1 to each tile the line covers
        start = min(x_1, x_2) if y_1 == y_2 else min(y_1, y_2)
        end = max(x_1, x_2) if y_1 == y_2 else max(y_1, y_2)
        while start <= end:
            if y_1 == y_2:
                self.board[y_1][start] += 1
            else:
                self.board[start][x_1] += 1
            start += 1

    def print_board(self):
        """Print the board in a useful fashion"""
        for row in self.board:
            print(str(row)[1:-1].replace('0', '.').replace(',', ''))

    def count_overlaps(self):
        """Return the number of tiles where 2 or more lines cross"""
        return sum(map(
            lambda row: len(list(filter(
                lambda x: x > 1,
                row
            ))),
            self.board
        ))

def main(filename):
    """The solution."""
    with open(filename, 'r') as file:
        vent_grid = VentGrid()
        for line in file:
            p_1, p_2 = line.strip().split(' -> ')
            x_1, y_1 = p_1.split(',')
            x_2, y_2 = p_2.split(',')
            vent_grid.add_line(int(x_1), int(y_1), int(x_2), int(y_2))
        # print()
        # vent_grid.print_board()
        print(f"\nThere are {vent_grid.count_overlaps()} overlaps.")

if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print(f"Usage: python {sys.argv[0]} <file>")
