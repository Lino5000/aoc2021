{-# LANGUAGE TupleSections #-}

import Data.Char (isDigit)
import Data.List (group, sort)

type Target = ((Int, Int), (Int, Int))

parse :: String -> Target
parse inp = ((x1, y1), (x2, y2))
  where
    as = drop 1 $ dropWhile (/= '=') inp
    (x1, bs) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') as
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') as)
    (x2, cs) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') bs
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') bs)
    ds = drop 1 $ dropWhile (/= '=') cs
    (y1, es) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') ds
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') ds)
    (y2, _) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') es
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') es)

possibleXVeloc :: Target -> [Int]
possibleXVeloc ((x1, _), (x2, _)) = [minV .. x2]
  where
    minV = ceiling $ (sqrt (1.0 + 8.0 * fromIntegral x1) - 1.0) / 2.0
    -- This came from rearranging `x1 <= sum([1..minV])`
    -- and taking the smallest positive integer option for `minV`

possibleTimes :: Target -> Int -> (Int, Maybe Int)
possibleTimes ((x1, _), (x2, _)) xV = (minT, maxT)
  where
    minT =
      ceiling $
      fromIntegral xV - 1.0 / 2.0 -
      sqrt ((fromIntegral xV - 1.0 / 2.0) ^ 2 - 2.0 * fromIntegral x2)
    maxT =
      if xs > x2
        then Just
               (floor $
                fromIntegral xV - 1.0 / 2.0 -
                sqrt ((fromIntegral xV - 1.0 / 2.0) ^ 2 - 2.0 * fromIntegral x2))
        else Nothing
    xs = xV * (xV + 1) `div` 2

possibleYVeloc :: Target -> (Int, Maybe Int) -> [Int]
possibleYVeloc ((_, y1), (_, y2)) (minT, Nothing) = helper y1 y2 minT []
  where
    yVfor :: RealFrac a => Int -> Int -> a
    yVfor yf f = fromIntegral yf / fromIntegral f + (fromIntegral f - 1.0) / 2.0
    helper :: Int -> Int -> Int -> [Int] -> [Int]
    helper y1 y2 f soFar
      | fromIntegral yV0 <= (-y1) - 1 = helper y1 y2 (f + 1) $ yV0 : soFar
      | otherwise = soFar
      where
        yV0 = floor $ yVfor y2 f
possibleYVeloc ((_, y1), (_, y2)) (minT, Just maxT) =
  map floor . filter isInt $ concatMap get_yVs [y1 .. y2]
  where
    get_yVs :: RealFrac a => Int -> [a]
    get_yVs y = map (yVfor y) [minT .. maxT]
    yVfor :: RealFrac a => Int -> Int -> a
    yVfor yf f = fromIntegral yf / fromIntegral f + (fromIntegral f - 1.0) / 2.0
    isInt :: RealFrac a => a -> Bool
    isInt x = floor x == ceiling x

possibleVeloc :: Target -> [(Int, Int)]
possibleVeloc target =
  concatMap
    ((\(xV, t) -> map (xV, ) $ possibleYVeloc target t) .
     (\xV -> (xV, possibleTimes target xV)))
    xVs
  where
    xVs = possibleXVeloc target

dedup :: Ord a => [a] -> [a]
dedup = map head . group . sort

maxHeight :: Int -> Int
maxHeight yV = yV * (yV + 1) `div` 2

solve :: String -> Int
solve = maximum . map (maxHeight . snd) . possibleVeloc . parse

main :: IO ()
main = getContents >>= print . solve
