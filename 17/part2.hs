{-# LANGUAGE TupleSections #-}

import Data.Bifunctor (bimap)
import Data.Char (isDigit)
import Data.List (group, sort)

type Target = ((Int, Int), (Int, Int))

isInt :: RealFrac a => a -> Bool
isInt a = floor a == ceiling a

parse :: String -> Target
parse inp = ((x1, y1), (x2, y2))
  where
    as = drop 1 $ dropWhile (/= '=') inp
    (x1, bs) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') as
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') as)
    (x2, cs) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') bs
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') bs)
    ds = drop 1 $ dropWhile (/= '=') cs
    (y1, es) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') ds
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') ds)
    (y2, _) =
      ( read $ takeWhile (\c -> isDigit c || c == '-') es
      , dropWhile (== '.') $ dropWhile (\c -> isDigit c || c == '-') es)

targetCoords :: Target -> [(Int, Int)]
targetCoords ((x1, y1), (x2, y2)) =
  concatMap (flip zip [y1 .. y2] . repeat) [x1 .. x2]

possibleTimes :: Int -> Int -> [Int]
possibleTimes xf yf = [1 .. maxT]
  where
    a = (-1.0 + sqrt (1.0 + 8 * fromIntegral xf)) / 2.0
    triX = isInt a
    maxT =
      if triX
        then -2 * yf
        else ceiling a

velocFor :: (Int, Int, Int) -> (Double, Double)
velocFor (xf, yf, f) = (xV, yV)
  where
    v1 :: Int -> Int -> Double
    v1 target time =
      (fromIntegral target / fromIntegral time) + fromIntegral (time - 1) / 2.0
    yV = v1 yf f
    xV1 = v1 xf f
    xV =
      if xV1 >= fromIntegral f
        then xV1
        else (-1.0 + sqrt (1.0 + 8 * fromIntegral xf)) / 2.0

dedup :: Ord a => [a] -> [a]
dedup = map head . group . sort

solve :: String -> Int
solve =
  length .
  dedup .
  map (bimap floor floor) .
  filter (\(x, y) -> isInt x && isInt y) .
  map velocFor .
  concatMap (\(x, y) -> map (x, y, ) $ possibleTimes x y) . targetCoords . parse

main :: IO ()
main = getContents >>= print . solve
