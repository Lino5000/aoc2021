use std::io::stdin;
use std::io::BufRead;

fn main() {
    let stdin = stdin();
    let stdin = stdin.lock();
    let mut counts: Vec<i32> = vec![];
    let mut line_count = 0;
    let lines = stdin.lines();
    for line in lines.map(|l| l.unwrap()) {
        let mut value = i32::from_str_radix(&line, 2).unwrap();
        let mut i: usize = 0;
        if line.len() > counts.len() {
            counts.resize(line.len(), 0);
        }
        while value > 0 {
            if value % 2 == 1 {
                counts[i] += 1;
            }
            value = value >> 1;
            i += 1;
        }
        line_count += 1;
    }

    counts.reverse();

    println!("{:?}", counts);

    let mut gamma = 0;
    let threshold: f32 = (line_count as f32) / 2.0;
    let max_val = 1 << counts.len() as i32;
    for count in counts {
        if count as f32 > threshold {
            gamma += 1;
        }
        gamma <<= 1;
    }
    gamma >>= 1;
    println!("Gamma = {}", gamma);
    let epsilon = max_val - gamma - 1;
    println!("Epsilon = {}", epsilon);

    println!("Solution = {}", gamma * epsilon);
}
