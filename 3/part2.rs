use std::io::stdin;
use std::io::BufRead;

fn calc_rating(values: &Vec<i32>, take_most_common: bool, line_length: usize) -> i32 {
    let mut candidates: Vec<i32> = values.clone();

    //println!();
    //println!("Oxygen? {}", take_most_common);
    //println!("Initial Candidates are {:?}", candidates);

    for col in (0..line_length).rev() {
        // First loop to determine which bit is more common.
        let mut ones = 0;
        for i in 0..candidates.len() {
            ones += (candidates[i] >> col) & 1;
        }

        // Determine which is more common.
        let more_common =
            if ones > candidates.len() as i32 / 2 || ones * 2 == candidates.len() as i32 {
                1
            } else {
                0
            };

        // Second loop to separate the candidates.
        // Changing the contents of the vector means this can't be a for loop.
        let mut i: usize = 1;
        while i <= candidates.len() {
            if (candidates[i-1] >> col) & 1 == more_common {
                if !take_most_common && candidates.len() > 1 {
                    candidates.swap_remove(i-1);
                    i -= 1;  // Check this spot again, there's a different number here.
                }
            } else {
                if take_most_common && candidates.len() > 1 {
                    candidates.swap_remove(i-1);
                    i -= 1;  // Check this spot again, there's a different number here.
                }
            }
            i += 1;
        }

        //println!("After column {}, with {} ones meaning most common was {}, the candidates are {:?}", line_length-col, ones,  more_common, candidates);

        if candidates.len() <= 1 {
            break;
        }
    }

    if candidates.len() > 1 {
        // Something went wrong, there should only be one answer.
        println!("{:?}", candidates);
        panic!();
    } else {
        candidates[0]
    }
}

fn main() {
    let stdin = stdin();
    let stdin = stdin.lock();
    let lines: Vec<String> = stdin.lines().filter_map(std::io::Result::ok).collect();

    // NOTE: This only works if all the lines are the same length, which for this problem they
    // should be.
    let line_length = lines[0].len();

    let values: Vec<i32> = lines.iter()
        .map(|l| i32::from_str_radix(l, 2))
        .filter_map(std::result::Result::ok)
        .collect();

    let co_rating = calc_rating(&values, false, line_length);
    println!("CO2 Rating = {}", co_rating);

    let ox_rating = calc_rating(&values, true, line_length);
    println!("O2 Rating = {}", ox_rating);

    println!();
    println!("Solution = {}", ox_rating * co_rating);
}
