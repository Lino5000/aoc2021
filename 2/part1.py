"""
Part 1 of Day 2 of the 2021 AOC.
"""

import sys

def main(filename):
    """The solution."""
    pos, depth = 0, 0
    with open(filename, 'r') as file:
        for line in file.readlines():
            (direction, distance) = line.split()
            if direction == "forward":
                pos += int(distance)
            elif direction == "down":
                depth += int(distance)
            elif direction == "up":
                depth -= int(distance)
            else:
                print(f"ERROR: Invalid command `{direction}`")
                return
    print(f"{pos = }, {depth = }")
    print(f"Solution = {pos * depth}")

if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print(f"Usage: python {sys.argv[0]} <file>")
