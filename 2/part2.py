"""
Part 2 of Day 2 of the 2021 AOC.
"""

import sys

def main(filename):
    """The solution."""
    pos, depth, aim = 0, 0, 0
    with open(filename, 'r') as file:
        for line in file.readlines():
            (direction, dist) = line.split()
            distance = int(dist)
            if direction == "forward":
                pos += distance
                depth += distance * aim
            elif direction == "down":
                aim += distance
            elif direction == "up":
                aim -= distance
            else:
                print(f"ERROR: Invalid command `{direction}`")
                return
    print(f"{pos = }, {depth = }, {aim = }")
    print(f"Solution = {pos * depth}")

if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print(f"Usage: python {sys.argv[0]} <file>")
