use std::io::stdin;
use std::io::BufRead;
use std::collections::{HashMap, HashSet};

fn get_key<K, V>(map: &HashMap<K, V>, value: &V) -> Option<K> 
    where
        V: PartialEq,
        K: Copy
{
    for (&key, val) in map.iter() {
        if val == value {
            return Some(key);
        }
    }
    None
}

fn solve_line(line: String) -> i32 {
    // Parse the input
    let mut pieces = line.split(" | ");
    let patterns = pieces.next().unwrap().split(" ");
    let output = pieces.next().unwrap().split(" ");

    let mut mapping: HashMap<i32, HashSet<char>> = HashMap::new();

    // Determine the obvious patterns (1, 4, 7, 8) and set placeholders for the others
    for pattern in patterns.clone() {
        let key = 
            match pattern.len() {
                2 => 1,
                3 => 7,
                4 => 4,
                7 => 8,
                _ => -1
            };
        if key > 0 {
            let value: HashSet<char> = pattern.chars().collect();
            mapping.insert(key, value);
        }
    }

    let bd: HashSet<char> = mapping[&4].difference(&mapping[&1]).map(|c| *c).collect();

    // Determine what the remaining patterns are based on which segments are included
    for pattern in patterns {
        let value: HashSet<char> = pattern.chars().collect();
        if !mapping.values().collect::<Vec<&HashSet<char>>>().contains(&&value) {
            mapping.insert(
                if pattern.len() == 6 {
                    // 0, 6, 9
                    if value.is_superset(&bd) {
                        if value.is_superset(&mapping[&1]) {
                            9
                        } else {
                            6
                        }
                    } else {
                            0
                    }
                } else {
                    // 2, 3, 5
                    if value.is_superset(&bd) {
                        5
                    } else {
                        if value.is_superset(&mapping[&1]) {
                            3
                        } else {
                            2
                        }
                    }
                },
                value
            );
        }
    }

    // Decode the output
    let mut out: i32 = 0;

    for pattern in output {
        let value: HashSet<char> = pattern.chars().collect();
        if let Some(digit) = get_key(&mapping, &value) {
            out = out * 10 + digit;
        }
    }
    
    return out;
}

fn main() {
    let stdin = stdin();
    let stdin = stdin.lock();
    let lines = stdin.lines();
    let mut total: i32 = 0;
    for line in lines.map(|l| l.unwrap()) {
        total += solve_line(line);
    }
    println!("Total = {}", total);
}
