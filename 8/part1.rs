use std::io::stdin;
use std::io::BufRead;

fn solve_line(line: String) -> i32 {
    let mut count = 0;
    if let Some(output_codes) = line.split(" | ").last() {
        for code in output_codes.split(" ") {
            count += match code.len() {
                2 | 4 | 3 | 7 => 1,
                _ => 0
            }
        }
    }
    return count;
}

fn main() {
    let stdin = stdin();
    let stdin = stdin.lock();
    let lines = stdin.lines();
    let mut count: i32 = 0;
    for line in lines.map(|l| l.unwrap()) {
        count += solve_line(line);
    }
    println!("Total count = {}", count);
}
