use std::fmt;
use std::io::stdin;
use std::io::BufRead;
use std::collections::HashMap;

#[derive(Default, Debug)]
struct Cave {
    is_big: bool,
    connected: Vec<String>, // The names of the connected caves.
}

impl Cave {
    fn big() -> Self {
        Self {
            is_big: true,
            ..Default::default()
        }
    }

    fn add_connection(&mut self, other_name: String) {
        self.connected.push(other_name);
    }
}

#[derive(Clone, Default)]
struct Path { steps: Vec<String> }

impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut out: String = "".to_string();
        for step in self.steps.iter() {
            out += &*format!("{}, ", step);
        }
        write!(f, "{}", out.trim_end_matches(", "))
    }
}

impl Path {
    fn get_duped_small(&self, system: &CaveSystem) -> Option<String> {
        let mut seen: Vec<String> = Vec::new();
        for name in self.steps.iter() {
            if !system.caves.get(name).unwrap().is_big && seen.contains(&name) {
                return Some(name.clone());
            } else {
                seen.push(name.clone());
            }
        }
        return None;
    }
}

#[derive(Debug)]
struct CaveSystem {
    caves: HashMap<String, Cave>,
}

impl CaveSystem {
    fn new() -> Self {
        let mut caves: HashMap<String, Cave> = HashMap::new();
        caves.insert("start".to_string(), Cave::default());
        caves.insert("end".to_string(), Cave::default());

        Self { caves }
    }

    fn add_cave(&mut self, name: String) -> &mut Cave {
        if !self.caves.contains_key(&name) {
            self.caves.insert(name.clone(),
                if name == name.to_uppercase() {
                    Cave::big()
                } else {
                    Cave::default()
                }
            );
        }
        self.caves.get_mut(&name).unwrap()
    }

    fn add_connection(&mut self, a: String, b: String) {
        let cave_a = self.add_cave(a.clone());
        cave_a.add_connection(b.clone());

        let cave_b = self.add_cave(b);
        cave_b.add_connection(a);
    }

    fn is_valid_addition(&self, next_cave_name: &String, prefix: Path) -> bool {
        if prefix.steps.len() > 0 && *next_cave_name == "start" {
            false
        } else {
            let is_big = self.caves.get(next_cave_name).unwrap().is_big;
            let duped = prefix.steps.contains(next_cave_name);
            let valid_dups =
                if let Some(_duped) = prefix.get_duped_small(&self) {
                    false
                } else {
                    true
                };

            is_big || !duped || valid_dups
        }
    }

    fn find_path_helper(&self, start: String, end: String, prefix: Path) -> Vec<Path> {
        if start != end {
            let mut paths: Vec<Path> = vec![];

            if let Some(start_cave) = self.caves.get(&start) {
                let mut next_prefix = prefix.clone();
                next_prefix.steps.push(start);
                for next_cave_name in start_cave.connected.iter() {
                    if self.is_valid_addition(next_cave_name, next_prefix.clone()) {
                        let possible_conts = self.find_path_helper(
                            next_cave_name.clone(), end.clone(), next_prefix.clone()
                        );
                        for cont in possible_conts.iter() {
                            paths.push(cont.clone());
                        }
                    }
                }
            }

            paths
        } else {
            let mut path = prefix.clone();
            path.steps.push(end);
            vec![path]
        }
    }

    fn find_paths(&self) -> Vec<Path> {
        self.find_path_helper("start".to_string(), "end".to_string(), Path::default())
    }
}

fn main() {
    let stdin = stdin();
    let stdin = stdin.lock();
    let lines = stdin.lines();

    let mut system = CaveSystem::new();

    for line in lines.map(|l| l.unwrap()) {
        let mut split = line.split("-");
        let name_a = split.next().unwrap();
        let name_b = split.next().unwrap();
        system.add_connection(name_a.to_string(), name_b.to_string());
    }

    println!("There are {} paths.", system.find_paths().len());
}
